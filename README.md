# dui: like du, but interactive

`dui` is a fast way to find which folders and files are using up your disk space

## Installation

`cargo install dui`

## Usage

```
Usage: dui [OPTIONS] [PATH]

Arguments:
  [PATH]
          The path to search. If empty, default to the current directory

          [default: .]

Options:
  -t, --type <DATA_TYPE>
          What type of data to display

          [default: file_size]

          Possible values:
          - file_size: Report the size (in bytes) of each file
          - num_files: Report the number of files
```

Launch an interactive terminal frame that allows you to explore your folders and files. If a path is provided, `dui` will report metadata (file sizes or number of files) for all folders and files in the subtree rooted at that path. If no path is provided, `dui` defaults to the current directory.

Once `dui` has started running, use the following keys to navigate:
- [Enter] to expand/collapse the currently selected folder
- up/down arrows (or ctrl-p/ctrl-n) to navigate up and down the folder tree
- q (or ctrl-c or ctrl-d) to quit

`dui` will ignore any folders and files ignored by `.gitignore`

## Example output

With `--type file_size` (the default setting):

```
┗⊟ ".": 27.2 KB (100.00%)
  ┣⊟ "src": 23.5 KB (86.48%)
  ┃ ┣⊟ "cli": 17.9 KB (76.35%)
  ┃ ┃ ┣⊡ "render.rs": 9.1 KB (50.55%)
  ┃ ┃ ┣⊡ "handler.rs": 5.3 KB (29.66%)
  ┃ ┃ ┗⊡ "mod.rs": 3.4 KB (18.92%)
  ┃ ┣⊡ "crawl.rs": 3.8 KB (16.38%)
  ┃ ┣⊡ "main.rs": 1.5 KB (6.36%)
  ┃ ┗⊡ "lib.rs": 28 B (0.12%)
  ┣⊡ "README.md": 1.1 KB (4.19%)
  ┣⊡ "LICENSE": 1 KB (3.82%)
  ┣⊡ "TODO.org": 453 B (1.63%)
  ┣⊡ "Cargo.toml": 451 B (1.62%)
  ┗⊡ "rustfmt.toml": 247 B (0.89%)
```

With `--type num_files`:

```
┗⊟ ".": 14 (100.00%)
  ┣⊟ "src": 8 (57.14%)
  ┃ ┣⊟ "cli": 4 (50.00%)
  ┃ ┃ ┣⊡ "render.rs": 1 (25.00%)
  ┃ ┃ ┣⊡ "handler.rs": 1 (25.00%)
  ┃ ┃ ┗⊡ "mod.rs": 1 (25.00%)
  ┃ ┣⊡ "main.rs": 1 (12.50%)
  ┃ ┣⊡ "lib.rs": 1 (12.50%)
  ┃ ┗⊡ "crawl.rs": 1 (12.50%)
  ┣⊡ "Cargo.toml": 1 (7.14%)
  ┣⊡ "TODO.org": 1 (7.14%)
  ┣⊡ "README.md": 1 (7.14%)
  ┣⊡ "LICENSE": 1 (7.14%)
  ┗⊡ "rustfmt.toml": 1 (7.14%)
```
