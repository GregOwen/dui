use std::path::PathBuf;
use std::{io, thread};

use clap::Parser;
use crossbeam_channel::RecvError;
use ignore::WalkBuilder;

use dui::{cli::CrosstermCli, crawl::DataType};
use dui::crawl::{FileSizeVisitorBuilder, PathSizeRecorder};

// TODO(greg): support multiple paths
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Args {
  /// The path to search. If empty, default to the current directory.
  #[arg(value_parser, default_value = ".")]
  path: PathBuf,
  /// What type of data to display.
  #[arg(short = 't', long = "type", value_enum, default_value = "file_size")]
  data_type: DataType,
}

fn main() {
  let args = Args::parse();
  let recorder_path = args.path.clone();

  let mut stdout = io::stdout();

  let (sender, receiver) = crossbeam_channel::unbounded();
  let mut visitor_builder = FileSizeVisitorBuilder::new(sender);

  let print_thread = thread::spawn(move || {
    let mut recorder = PathSizeRecorder::new(recorder_path, args.data_type);
    loop {
      match receiver.recv() {
        Ok(entry) => recorder.merge_entry(entry),
        Err(RecvError) => break,
      }
    }
    let cli = CrosstermCli::from_recorder(recorder);
    cli.run(&mut stdout).unwrap();
  });

  WalkBuilder::new(args.path)
    .build_parallel()
    .visit(&mut visitor_builder);

  // Need to drop visitor_builder before joining the print thread so that the
  // Senders all drop, the channel closes, and we know we can print the results
  drop(visitor_builder);

  print_thread.join().unwrap();
}
