use std::io::Write;

use crossterm::terminal::{self, ClearType};
use crossterm::{cursor, execute, queue, style, Result};

use crate::{cli::{CrosstermCli, TreeNode}, crawl::data_string};

const LAST_CHILD_INDENT: &str = "  ";
const MIDDLE_CHILD_INDENT: &str = "┃ ";

const UNEXPANDED: char = '⊞';
const EXPANDED: char = '⊟';
const NO_CHILDREN: char = '⊡';
const MIDDLE_BULLET: char = '┣';
const END_BULLET: char = '┗';

impl TreeNode {
  fn render<W>(
    &self,
    w: &mut W,
    prefix: &String,
    is_last_child: bool,
    parent_size: Option<f64>,
    // Path, relative to this node, of the top node on the screen. If top_path
    // is empty, this node is below the top of the screen. If top_path is
    // nonempty, this node is above the top of the screen and the first element
    // of top_path is the last child of this node that is above the top of the
    // screen.
    top_path: &[usize],
    // Number of rows in the screen
    screen_rows: u16,
    // Number of rows rendered so far in this render pass. If this is greater
    // than the screen size we should not render any more rows.
    rows_rendered: &mut u16,
  ) -> Result<()>
  where
    W: Write,
  {
    if *rows_rendered >= screen_rows {
      return Ok(());
    }

    // We're truncating the value to make it human-readable anyways, so it's
    // fine to lose precision here. u64 as f64 can't overflow, so we'll always
    // get a reasonable value.
    let float_size = self.size as f64;

    if top_path.is_empty() {
      let bullet = if is_last_child {
        END_BULLET
      } else {
        MIDDLE_BULLET
      };
      let expand_toggle = if self.children.is_empty() {
        NO_CHILDREN
      } else if self.is_expanded {
        EXPANDED
      } else {
        UNEXPANDED
      };
      let size_str = data_string(self.data_type, self.size);
      let size_percent = if let Some(parent_size) = parent_size {
        100.0 * float_size / parent_size
      } else {
        // If this node is the root, print 100%
        100.0
      };
      queue!(
        w,
        style::Print(format!(
          "{prefix}{bullet}{expand_toggle} {:?}: {size_str} ({:0.2}%)",
          self.path, size_percent,
        )),
        cursor::MoveToNextLine(1),
      )?;
      *rows_rendered += 1;
    }

    let mut child_prefix = prefix.clone();
    if is_last_child {
      child_prefix.extend(LAST_CHILD_INDENT.chars());
    } else {
      child_prefix.extend(MIDDLE_CHILD_INDENT.chars());
    }
    let (to_skip, child_top_path) = if top_path.is_empty() {
      (0, top_path)
    } else {
      (top_path[0], &top_path[1..])
    };

    if self.is_expanded {
      let mut child_iter = self.children.iter().enumerate().skip(to_skip);
      if let Some((first_i, first_c)) = child_iter.next() {
        // For the first child, we need to follow the rest of top_path
        first_c.render(
          w,
          &child_prefix,
          first_i == self.children.len() - 1,
          Some(float_size),
          child_top_path,
          screen_rows,
          rows_rendered,
        )?;
        // For the second child, we can drop top_path entirely
        for (i, c) in child_iter {
          c.render(
            w,
            &child_prefix,
            i == self.children.len() - 1,
            Some(float_size),
            &[],
            screen_rows,
            rows_rendered,
          )?;
        }
      }
    }
    Ok(())
  }
}

impl CrosstermCli {
  pub(super) fn render<W>(&self, w: &mut W) -> Result<()>
  where
    W: Write,
  {
    queue!(
      w,
      terminal::Clear(ClearType::All),
      // style::Print will start writing from the current position of the
      // cursor, so we need to reset it to the top-left corner before rendering
      // the tree
      cursor::MoveTo(0, 0),
    )?;
    let (_, screen_rows) = terminal::size()?;
    let mut rows_rendered = 0;
    self.tree.render(
      w,
      &String::new(),
      true,
      None,
      &self.top_path[..],
      screen_rows,
      &mut rows_rendered,
    )?;
    queue!(w, self.cursor_pos)?;
    w.flush()?;
    Ok(())
  }

  pub(super) fn render_cursor<W>(&self, w: &mut W) -> Result<()>
  where
    W: Write,
  {
    execute!(w, self.cursor_pos)
  }
}

#[cfg(test)]
mod tests {
  use regex::Regex;

  use crate::cli::INDENT_LEN;
  use crate::crawl::DataType;

  use super::*;

  // NOTE: the size attributed to a directory node is always slightly greater
  // than the sum of the sizes of the files in the directory because the
  // directory node itself has nonzero size
  const FULL_RENDER: [&str; 14] = [
    r#"┗⊟ ".": 24.1 KB (100.00%)"#,
    r#"  ┣⊟ "src": 21.1 KB (87.76%)"#,
    r#"  ┃ ┣⊟ "cli": 16.7 KB (79.08%)"#,
    r#"  ┃ ┃ ┣⊡ "render.rs": 8.2 KB (49.28%)"#,
    r#"  ┃ ┃ ┣⊡ "handler.rs": 5 KB (29.82%)"#,
    r#"  ┃ ┃ ┗⊡ "mod.rs": 3.3 KB (19.97%)"#,
    r#"  ┃ ┣⊡ "crawl.rs": 3 KB (14.21%)"#,
    r#"  ┃ ┣⊡ "main.rs": 1.4 KB (6.43%)"#,
    r#"  ┃ ┗⊡ "lib.rs": 28 B (0.13%)"#,
    r#"  ┣⊡ "LICENSE": 1 KB (4.31%)"#,
    r#"  ┣⊡ "README.md": 757 B (3.07%)"#,
    r#"  ┣⊡ "Cargo.toml": 453 B (1.84%)"#,
    r#"  ┣⊡ "TODO.org": 273 B (1.11%)"#,
    r#"  ┗⊡ "rustfmt.toml": 247 B (1.00%)"#,
  ];

  fn full_tree() -> TreeNode {
    TreeNode {
      path: ".".into(),
      data_type: DataType::FileSize,
      size: 24_631,
      is_expanded: true,
      children: vec![
        TreeNode {
          path: "src".into(),
          data_type: DataType::FileSize,
          size: 21_615,
          is_expanded: true,
          children: vec![
            TreeNode {
              path: "cli".into(),
              data_type: DataType::FileSize,
              size: 17_093,
              is_expanded: true,
              children: vec![
                TreeNode {
                  path: "render.rs".into(),
                  data_type: DataType::FileSize,
                  size: 8_423,
                  is_expanded: false,
                  children: vec![],
                },
                TreeNode {
                  path: "handler.rs".into(),
                  data_type: DataType::FileSize,
                  size: 5_097,
                  is_expanded: false,
                  children: vec![],
                },
                TreeNode {
                  path: "mod.rs".into(),
                  data_type: DataType::FileSize,
                  size: 3_413,
                  is_expanded: false,
                  children: vec![],
                },
              ],
            },
            TreeNode {
              path: "crawl.rs".into(),
              data_type: DataType::FileSize,
              size: 3_072,
              is_expanded: false,
              children: vec![],
            },
            TreeNode {
              path: "main.rs".into(),
              data_type: DataType::FileSize,
              size: 1_390,
              is_expanded: false,
              children: vec![],
            },
            TreeNode {
              path: "lib.rs".into(),
              data_type: DataType::FileSize,
              size: 28,
              is_expanded: false,
              children: vec![],
            },
          ],
        },
        TreeNode {
          path: "LICENSE".into(),
          data_type: DataType::FileSize,
          size: 1_062,
          is_expanded: false,
          children: vec![],
        },
        TreeNode {
          path: "README.md".into(),
          data_type: DataType::FileSize,
          size: 757,
          is_expanded: false,
          children: vec![],
        },
        TreeNode {
          path: "Cargo.toml".into(),
          data_type: DataType::FileSize,
          size: 453,
          is_expanded: false,
          children: vec![],
        },
        TreeNode {
          path: "TODO.org".into(),
          data_type: DataType::FileSize,
          size: 273,
          is_expanded: false,
          children: vec![],
        },
        TreeNode {
          path: "rustfmt.toml".into(),
          data_type: DataType::FileSize,
          size: 247,
          is_expanded: false,
          children: vec![],
        },
      ],
    }
  }

  #[test]
  fn test_indent_len() {
    assert_eq!(LAST_CHILD_INDENT.chars().count(), INDENT_LEN as usize);
    assert_eq!(MIDDLE_CHILD_INDENT.chars().count(), INDENT_LEN as usize);
  }

  #[test]
  fn test_basic_render() -> Result<()> {
    let mut lines = vec![];
    let cli = CrosstermCli {
      tree: full_tree(),
      current_path: vec![],
      cursor_pos: cursor::MoveTo(1, 0),
      top_path: vec![],
    };
    cli.render(&mut lines)?;
    // render() will also write ASCII control characters (e.g. "move cursor to
    // this location"), so we need to filter those out before comparing to the
    // expected result. We do this using a regex.
    // See https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_(Control_Sequence_Introducer)_sequences
    // and https://stackoverflow.com/a/14693789
    let non_control_re = Regex::new(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])").unwrap();
    let lines_str = std::str::from_utf8(&lines).unwrap();
    let non_control_lines_str = non_control_re.replace_all(lines_str, "");

    assert_eq!(non_control_lines_str, FULL_RENDER.join(""));
    Ok(())
  }
}
