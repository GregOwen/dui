use std::{
  collections::HashMap, ffi::OsString, fmt, fmt::Display, path::PathBuf,
};

use clap::ValueEnum;
use crossbeam_channel::Sender;
use human_bytes::human_bytes;
use ignore::{
  DirEntry, Error, ParallelVisitor, ParallelVisitorBuilder, WalkState,
};

struct FileSizeVisitor {
  sender: Sender<DirEntry>,
}

impl FileSizeVisitor {
  pub fn new(sender: Sender<DirEntry>) -> FileSizeVisitor {
    FileSizeVisitor { sender }
  }
}

impl ParallelVisitor for FileSizeVisitor {
  fn visit(&mut self, entry: Result<DirEntry, Error>) -> WalkState {
    match entry {
      Ok(entry) => self.sender.send(entry).unwrap(),
      Err(err) => println!("ERROR: {}", err),
    }
    WalkState::Continue
  }
}

pub struct FileSizeVisitorBuilder {
  sender: Sender<DirEntry>,
}

impl FileSizeVisitorBuilder {
  pub fn new(sender: Sender<DirEntry>) -> FileSizeVisitorBuilder {
    FileSizeVisitorBuilder { sender }
  }
}

impl<'s> ParallelVisitorBuilder<'s> for FileSizeVisitorBuilder {
  fn build(&mut self) -> Box<dyn ParallelVisitor + 's> {
    Box::new(FileSizeVisitor::new(self.sender.clone()))
  }
}

pub struct PathSizeRecord {
  pub size: u64,
  pub data_type: DataType,
  pub children: HashMap<OsString, PathSizeRecord>,
}

impl PathSizeRecord {
  fn new(data_type: DataType) -> Self {
    PathSizeRecord {
      size: 0,
      data_type,
      children: HashMap::default(),
    }
  }

  fn display(
    &self,
    f: &mut fmt::Formatter<'_>,
    indent_level: usize,
  ) -> fmt::Result {
    let mut indent = String::with_capacity(indent_level);
    for _ in 0..indent_level {
      indent.push('\t');
    }
    let size_str = data_string(self.data_type, self.size);
    write!(f, "{size_str}\n")?;
    // TODO(greg): cache this value
    let sorted_children = &mut self.children.iter().collect::<Vec<_>>();
    sorted_children
      .sort_by(|(_, a_rec), (_, b_rec)| b_rec.size.cmp(&a_rec.size));
    for (c, rec) in sorted_children.iter() {
      write!(f, "{}{:?}: ", indent, c)?;
      rec.display(f, indent_level + 1)?;
    }
    Ok(())
  }
}

impl Display for PathSizeRecord {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    self.display(f, 0)
  }
}

#[derive(Clone, Copy, Debug, ValueEnum)]
#[clap(rename_all = "snake_case")]
pub enum DataType {
  /// Report the size (in bytes) of each file
  FileSize,
  /// Report the number of files
  NumFiles,
}

pub fn data_string(data_type: DataType, size: u64) -> String {
  match data_type {
    DataType::FileSize => human_bytes(size as f64),
    DataType::NumFiles => size.to_string(), // TODO(greg): make this human-readable too
  }
}

pub struct PathSizeRecorder {
  pub root: PathBuf,
  pub data_type: DataType,
  pub data: PathSizeRecord,
}

impl PathSizeRecorder {
  pub fn new(root: PathBuf, data_type: DataType) -> PathSizeRecorder {
    PathSizeRecorder {
      root,
      data_type,
      data: PathSizeRecord::new(data_type),
    }
  }

  pub fn merge_entry(&mut self, entry: DirEntry) {
    let size = get_size(self.data_type, &entry);
    self.data.size += size;
    let mut curr_children = &mut self.data.children;
    let components =
      entry.path().strip_prefix(&self.root).unwrap().components();
    for c in components {
      let child_record = curr_children
        .entry(c.as_os_str().into())
        .or_insert(PathSizeRecord::new(self.data_type));
      child_record.size += size;
      curr_children = &mut child_record.children;
    }
  }
}

fn get_size(data_type: DataType, entry: &DirEntry) -> u64 {
  match data_type {
    DataType::FileSize => match entry.metadata() {
      Ok(md) => md.len(),
      Err(err) => {
        println!("{}: {}", entry.path().display(), err.to_string());
        return 0;
      },
    },
    DataType::NumFiles => 1,
  }
}

impl Display for PathSizeRecorder {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "\"{}\": ", self.root.display())?;
    self.data.display(f, 1)
  }
}
